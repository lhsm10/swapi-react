const defaultState = {
  next: "",
  previous: "",
  loading: true,
  error: "",
  people: [],
  favoritePeople: [],
};

const peopleReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "ADD_TO_FAV":
      const { favedPeople } = action;
      return {
        ...state,
        favoritePeople: [...state.favoritePeople, favedPeople],
      };

    case "DELETE_FAV_PEOPLE":
      const { key } = action;
      console.log(key);
      const { favoritePeople } = state;
      favoritePeople.splice(key, 1);
      console.log(favoritePeople);
      return {
        ...state,
        favoritePeople: [...favoritePeople],
      };

    case "GET_PEOPLE_REQUEST":
      return {
        ...state,
        loading: true,
      };

    case "GET_PEOPLE_SUCCESS":
      const { people, next, prev } = action;
      return {
        ...state,
        next: next,
        previous: prev,
        loading: false,
        people: people,
        error: "",
      };
    case "GET_PEOPLE_ERROR":
      return {
        ...state,
        loading: false,
        people: [],
        error: "Ooops, to bad! Consult the droids!",
      };
    case "GET_NEXT_PAGE":
      return {
        ...state,
        loading: true,
      };

    case "NEXT_PAGE_SUCCESS":
      const { nextPagePeople, nextPageUrl, prevPageUrl } = action;
      return {
        ...state,
        next: nextPageUrl,
        previous: prevPageUrl,
        loading: false,
        people: nextPagePeople,
        error: "",
      };

    case "GET_PREV_PAGE":
      return {
        ...state,
        loading: true,
      };

    case "PREV_PAGE_SUCCESS":
      const { prevPagePeople, nextPageUrl2, prevPageUrl2 } = action;
      return {
        ...state,
        next: nextPageUrl2,
        previous: prevPageUrl2,
        loading: false,
        people: prevPagePeople,
        error: "",
      };

    default:
      return state;
  }
};

export default peopleReducer;
