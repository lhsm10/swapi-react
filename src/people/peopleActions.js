import axios from "axios";

export const AddToFav = (favedPeople) => ({ type: "ADD_TO_FAV", favedPeople });

export const deleteFavPeople = (key) => ({ type: "DELETE_FAV_PEOPLE", key });

export const getPeopleRequest = () => ({ type: "GET_PEOPLE_REQUEST" });

export const getPeopleSuccess = (people, next, prev) => ({
  type: "GET_PEOPLE_SUCCESS",
  people,
  next,
  prev,
});

export const getPeopleError = (error) => ({
  type: "GET_PEOPLE_FAILURE",
  error: error,
});

export const nextPageRequest = () => ({ type: "GET_NEXT_PAGE" });

export const nextPageSuccess = (nextPagePeople, nextPageUrl, prevPageUrl) => ({
  type: "NEXT_PAGE_SUCCESS",
  nextPagePeople,
  nextPageUrl,
  prevPageUrl,
});

export const prevPageRequest = () => ({ type: "GET_PREV_PAGE" });

export const prevPageSuccess = (
  prevPagePeople,
  nextPageUrl2,
  prevPageUrl2
) => ({
  type: "PREV_PAGE_SUCCESS",
  prevPagePeople,
  nextPageUrl2,
  prevPageUrl2,
});

export const getFirstPage = () => {
  return (dispatch) => {
    dispatch(getPeopleRequest);
    axios
      .get("http://swapi.dev/api/people/")
      .then((res) => {
        console.log(res);
        const people = res.data.results;
        const next = res.data.next;
        const prev = res.data.previous;
        dispatch(getPeopleSuccess(people, next, prev));
      })
      .catch((error) => {
        const errorMsg = error.message;
        dispatch(getPeopleError(errorMsg));
      });
  };
};

export const getNextPage = (nextPage) => {
  return (dispatch) => {
    dispatch(nextPageRequest);
    axios.get(nextPage).then((res) => {
      console.log(res);
      const PagePeople = res.data.results;
      const nextPageUrl = res.data.next;
      const prevPageUrl = res.data.previous;
      dispatch(nextPageSuccess(PagePeople, nextPageUrl, prevPageUrl));
    });
  };
};

export const getPrevPage = (prevPage) => {
  return (dispatch) => {
    dispatch(prevPageRequest);
    axios.get(prevPage).then((res) => {
      console.log(res);
      const prevPagePeople = res.data.results;
      const nextPageUrl2 = res.data.next;
      const prevPageUrl2 = res.data.previous;
      dispatch(prevPageSuccess(prevPagePeople, nextPageUrl2, prevPageUrl2));
    });
  };
};
