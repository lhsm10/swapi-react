import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { deleteFavPeople } from "./peopleActions";
const FavList = (props) => {
  const dispatch = useDispatch();

  const handleDelete = (key) => {
    dispatch(deleteFavPeople(key));
  };

  console.log(props);
  return (
    <div style={{ border: "white 2px solid" }}>
      <p>Key:{props.index}</p>
      <p>Name: {props.name}</p>
      <button
        onClick={() => {
          handleDelete(props.index);
        }}
      >
        Remove from fav
      </button>
    </div>
  );
};

export default FavList;
