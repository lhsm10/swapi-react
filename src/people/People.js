import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFirstPage, getNextPage, getPrevPage } from "./peopleActions";
import PeopleCard from "./peopleCards";
import FavList from "./favList";

const People = () => {
  const people = useSelector((state) => state.people);
  const nextP = useSelector((state) => state.next);
  const prevP = useSelector((state) => state.previous);
  const error = useSelector((state) => state.error);
  const loading = useSelector((state) => state.loading);
  const faved = useSelector((state) => state.favoritePeople);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFirstPage());
  }, [dispatch]);

  const [input, setInput] = useState("");
  const [filteredPeople, setFilteredPeople] = useState([]);

  const handleInput = (e) => {
    setInput(e.target.value);
  };

  useEffect(() => {
    console.log(input);
    console.log(people);
    const filtered = people.filter((person) => {
      person.name.includes(input);
    });
    setFilteredPeople([...filtered]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [input]);

  useEffect(() => {
    console.log(people);
    setFilteredPeople([...people]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [people]);

  console.log(faved);
  if (loading === true) {
    return <h2>Currently Loading...</h2>;
  } else if (error.error) {
    return { error };
  } else {
    return (
      <div>
        <input
          type="text"
          placeholder="Pesquise pessoas"
          value={input}
          onChange={handleInput}
        ></input>
        {filteredPeople.map((person) => (
          <PeopleCard name={person.name} height={person.height} />
        ))}
        <div>
          <button
            onClick={() => {
              dispatch(getNextPage(nextP));
            }}
          >
            Next
          </button>
          <button
            onClick={() => {
              dispatch(getPrevPage(prevP));
            }}
          >
            Previous
          </button>
          <div>
            {faved.map((favorite, key) => (
              <FavList
                index={key}
                key={key}
                name={favorite.name}
                eyes={favorite.eye_color}
              />
            ))}
            Total Favorites: {faved.reduce((acc) => acc + 1, 0)}
          </div>
        </div>
      </div>
    );
  }
};

export default People;
