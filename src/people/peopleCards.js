import React from "react";
import "./peopleCards.css";
import { useDispatch } from "react-redux";
import { AddToFav } from "./peopleActions";

const PeopleCard = (props) => {
  const dispatch = useDispatch();

  const handleAdd = (people) => {
    dispatch(AddToFav(people));
  };

  return (
    <div class="peopleWrapper">
      <p>Name: {props.name}</p>
      <p>Height: {props.height} pounds</p>
      <p></p>
      <p></p>
      <button onClick={() => handleAdd(props)}>Add to Favs</button>
    </div>
  );
};

export default PeopleCard;
