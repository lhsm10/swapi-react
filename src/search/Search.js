import React, { Component } from "react";
import axios from "axios";

class SwapiSearch extends Component {
  state = {
    search: [],
    searchQuery: "",
  };

  handleSearch = (e) => {
    this.setState({ searchQuery: e.target.value });
  };

  searchSwapi = (e) => {
    const { searchQuery } = this.state.searchQuery;

    e.preventDefault();
    const s = searchQuery;
    const searchUrl = `https://swapi.dev/api/people/?search=${s}`;
    if (this.state.searchQuery === undefined) {
      alert("Insira algo na pesquisa!");
    } else {
      axios.get(searchUrl).then((res) => console.log(res));
    }
  };

  render() {
    return (
      <div>
        <input
          onChange={this.handleSearch}
          value={this.state.searchQuery}
          placeholder="Pesquise na SWAPI"
        ></input>
        <button>PESQUISAR</button>
      </div>
    );
  }
}

export default SwapiSearch;
