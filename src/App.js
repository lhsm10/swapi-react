import React, { Component } from "react";
import "./App.css";
import "./links.css";
import { withRouter, Switch, Link, Route } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { withStyles } from "@material-ui/core/styles";
import useSound from "use-sound";
import People from "./people/People";
import SwapiSearch from "./search/Search";

const useStyles = (theme) => ({
  topbar: {
    background: "#9c9897",
    marginTop: "0 auto",
    width: "100%",
    position: "fixed",
    boxShadow: "2px 2px 2px red",
  },
  toolbar: {
    display: "flex",
    justifyContent: "center",
  },
});

class App extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className="App">
        <header className="App-header">
          <AppBar className={classes.topbar}>
            <Toolbar className={classes.toolbar}>
              <Link to="/people" class="barlink">
                People
              </Link>
              <Link to="/planets" class="barlink">
                Planets
              </Link>
              <Link to="starships" class="barlink">
                Starships
              </Link>
              <Link to="search" class="barlink">
                Search
              </Link>
            </Toolbar>
          </AppBar>
          <h1 style={{ color: "yellow" }}>STAR WARS API</h1>
          <Switch>
            <Route path="/people">
              <People />
            </Route>
            <Route path="/planets"></Route>
            <Route path="/starships"></Route>
            <Route path="/search"><SwapiSearch /></Route>
          </Switch>
        </header>
      </div>
    );
  }
}

export default withRouter(withStyles(useStyles)(App));
